class aptitude::install{
  package { 'aptitude':
    ensure => installed,
  }
}

class aptitude::remove{
  package {'aptitude':
    ensure => absent,
  }
}

