class vim::install{
  package { 'vim':
    ensure => installed,
  }
}

class vim::remove{
  package {'vim':
    ensure => absent,
  }
}